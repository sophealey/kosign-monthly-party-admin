package com.kosign.kmp.repositories;

import com.kosign.kmp.models.Location;
import com.kosign.kmp.models.LocationImage;
import com.kosign.kmp.models.LocationSelect;

import java.util.List;

public interface LocationRepository {
    List<Location> getLocatoinList(int limit,int page);
    String insertLocation(Location location);
    String updateLocation(Location location);
    String deleteLocation(String locationUuid);
    String deleteLocationImage(String locationUuid);
    String restoreLocation(String locationUuid);
    int countLoaction();
    List<LocationSelect> getAllLocation();
    List<LocationImage> getLocationImages(String locationUuid);
    String addLocationTag(Location location);

}
