package com.kosign.kmp.repositories;

import com.kosign.kmp.models.Event;

import java.util.List;

public interface EventRepository {
    List<Event> getEventList(String lmsDepartmentUUId, int limit, int page);
    int count(String dept_uuid);
    int countAllEvent();
    String toggleEventStatus(String uuid);
    int countDepartment();
}
