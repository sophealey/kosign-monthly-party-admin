package com.kosign.kmp.repositories;

import com.kosign.kmp.models.Category;
import com.kosign.kmp.models.MainCategory;
import sun.applet.Main;

import java.util.List;

public interface CategoryRepository {
    List<Category> getCategoryList(int limit, int page);
    List<Category> getAllSubcategories();
    List<MainCategory> getAllMaincategories();
    String insertCategory(Category category);
    String updateCategory(Category category);
    String toggleCategoryStatus(String categoryUuid);
    int count();
    String moveCategory(String mainCategoryUuid, String subcategoryUuid);

}
