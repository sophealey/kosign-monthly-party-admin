package com.kosign.kmp.repositories;

import com.kosign.kmp.models.Tag;

import java.util.List;

public interface TagRepository {
    List<Tag> getTagList(int limit, int page);
    String updateTage(Tag tag);
    String toggleTagStatus(String uuid);
    int count();
}
