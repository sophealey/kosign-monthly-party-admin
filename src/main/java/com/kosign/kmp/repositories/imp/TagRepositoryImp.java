package com.kosign.kmp.repositories.imp;

import com.kosign.kmp.models.Event;
import com.kosign.kmp.models.Tag;
import com.kosign.kmp.repositories.TagRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@Repository
public class TagRepositoryImp implements TagRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Tag> getTagList(int limit, int page) {
        List<Tag> tagList = new ArrayList<>();

        StoredProcedureQuery storedProcedureQuery = entityManager.createStoredProcedureQuery("fn_admin_get_tags",Tag.class)
                .registerStoredProcedureParameter("ipage", Integer.class, ParameterMode.IN)
                .registerStoredProcedureParameter("ilimit", Integer.class, ParameterMode.IN)
                .setParameter("ipage",page)
                .setParameter("ilimit",limit);

        tagList = storedProcedureQuery.getResultList();
        return tagList;
    }

    @Override
    @Transactional
    public String updateTage(Tag tag) {
        StoredProcedureQuery storedProcedureQuery = entityManager.createStoredProcedureQuery("fn_admin_update_tag")
                .registerStoredProcedureParameter("iname", String.class, ParameterMode.IN)
                .registerStoredProcedureParameter("iuuid", String.class, ParameterMode.IN)
                .registerStoredProcedureParameter("actioncode", String.class, ParameterMode.OUT)
                .setParameter("iname", tag.getTagName())
                .setParameter("iuuid", tag.getUuid());
        String actionCode = (String) storedProcedureQuery.getOutputParameterValue("actioncode");
        entityManager.clear();
        return actionCode;
    }

    @Override
    @Transactional
    public String toggleTagStatus(String uuid) {
        StoredProcedureQuery storedProcedureQuery = entityManager.createStoredProcedureQuery("fn_admin_toggle_tag_status")
                .registerStoredProcedureParameter("iuuid", String.class, ParameterMode.IN)
                .registerStoredProcedureParameter("actioncode", String.class, ParameterMode.OUT)
                .setParameter("iuuid", uuid);
        String actionCode = (String) storedProcedureQuery.getOutputParameterValue("actioncode");
        entityManager.clear();
        return actionCode;
    }

    @Override
    @Transactional
    public int count() {
        int countLocation = 0;
        Query storedProcedureQuery = entityManager.createNativeQuery("select count(*) from tag where status = '0'");
        countLocation = ((BigInteger) storedProcedureQuery.getSingleResult()).intValue();
        entityManager.clear();
        return countLocation;
    }
}
