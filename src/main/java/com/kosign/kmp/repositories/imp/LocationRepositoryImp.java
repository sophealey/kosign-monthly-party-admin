package com.kosign.kmp.repositories.imp;

import com.kosign.kmp.models.Location;
import com.kosign.kmp.models.LocationImage;
import com.kosign.kmp.models.LocationSelect;
import com.kosign.kmp.repositories.LocationRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@Repository
public class LocationRepositoryImp implements LocationRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Location> getLocatoinList(int limit, int page) {
        List<Location> locationList = new ArrayList<>();
        StoredProcedureQuery storedProcedureQuery = entityManager.createStoredProcedureQuery("fn_admin_get_location_with_tags",Location.class)
                .registerStoredProcedureParameter("ipage",Integer.class,ParameterMode.IN)
                .registerStoredProcedureParameter("ilimit",Integer.class,ParameterMode.IN)
                .setParameter("ipage",page)
                .setParameter("ilimit",limit);
        try {
            locationList = storedProcedureQuery.getResultList();
        }catch (Exception ex){
            ex.printStackTrace();
        }
        entityManager.clear();
        return locationList;
    }

    @Override
    @Transactional
    public String insertLocation(Location location) {
        StoredProcedureQuery storedProcedureQuery = entityManager.createStoredProcedureQuery("fn_admin_insert_location")
                .registerStoredProcedureParameter("iplace_name",String.class,ParameterMode.IN)
                .registerStoredProcedureParameter("iaddress",String.class,ParameterMode.IN)
                .registerStoredProcedureParameter("iwebsite_url",String.class,ParameterMode.IN)
                .registerStoredProcedureParameter("ilocation_name",String.class,ParameterMode.IN)
                .registerStoredProcedureParameter("icontact",String.class,ParameterMode.IN)
                .registerStoredProcedureParameter("iemail",String.class,ParameterMode.IN)
                .registerStoredProcedureParameter("ilongitude",Double.class,ParameterMode.IN)
                .registerStoredProcedureParameter("ilatitude",Double.class,ParameterMode.IN)
                .registerStoredProcedureParameter("irate",Double.class,ParameterMode.IN)
                .registerStoredProcedureParameter("actioncode",String.class,ParameterMode.OUT)
                .setParameter("iplace_name",location.getPlace_name())
                .setParameter("iaddress",location.getAddress())
                .setParameter("iwebsite_url",location.getWebsiteUrl())
                .setParameter("ilocation_name",location.getLocationName())
                .setParameter("icontact",location.getContact())
                .setParameter("iemail",location.getEmail())
                .setParameter("ilongitude",location.getLongitude())
                .setParameter("ilatitude",location.getLatitude())
                .setParameter("irate",location.getRate());

        String actionCode = (String) storedProcedureQuery.getOutputParameterValue("actioncode");
        storedProcedureQuery.executeUpdate();
        entityManager.clear();
        return actionCode;
    }

    @Override
    @Transactional
    public String updateLocation(Location location) {
        StoredProcedureQuery storedProcedureQuery = entityManager.createStoredProcedureQuery("fn_admin_update_location")
                .registerStoredProcedureParameter("iplace_name",String.class,ParameterMode.IN)
                .registerStoredProcedureParameter("iaddress",String.class,ParameterMode.IN)
                .registerStoredProcedureParameter("iwebsite_url",String.class,ParameterMode.IN)
                .registerStoredProcedureParameter("ilocation_name",String.class,ParameterMode.IN)
                .registerStoredProcedureParameter("icontact",String.class,ParameterMode.IN)
                .registerStoredProcedureParameter("iemail",String.class,ParameterMode.IN)
                .registerStoredProcedureParameter("ilongitude",Double.class,ParameterMode.IN)
                .registerStoredProcedureParameter("ilatitude",Double.class,ParameterMode.IN)
                .registerStoredProcedureParameter("irate",Double.class,ParameterMode.IN)
                .registerStoredProcedureParameter("iuuid",String.class,ParameterMode.IN)
                .registerStoredProcedureParameter("actioncode",String.class,ParameterMode.OUT)
                .setParameter("iplace_name",location.getPlace_name())
                .setParameter("iaddress",location.getAddress())
                .setParameter("iwebsite_url",location.getWebsiteUrl())
                .setParameter("ilocation_name",location.getLocationName())
                .setParameter("icontact",location.getContact())
                .setParameter("iemail",location.getEmail())
                .setParameter("ilongitude",location.getLongitude())
                .setParameter("ilatitude",location.getLatitude())
                .setParameter("irate",location.getRate())
                .setParameter("iuuid",location.getUuid());
        String actionCode = (String) storedProcedureQuery.getOutputParameterValue("actioncode");
        entityManager.clear();
        return actionCode;
    }

    @Override
    @Transactional
    public String deleteLocation(String locationUuid) {
        StoredProcedureQuery storedProcedureQuery = entityManager.createStoredProcedureQuery("fn_admin_delete_location")
                .registerStoredProcedureParameter("ilocationuuid",String.class,ParameterMode.IN)
                .registerStoredProcedureParameter("actioncode",String.class,ParameterMode.OUT)
                .setParameter("ilocationuuid",locationUuid);
        String actionCode = (String) storedProcedureQuery.getOutputParameterValue("actioncode");
        entityManager.clear();
        return actionCode;
    }

    @Override
    @Transactional
    public String deleteLocationImage(String locationUuid) {
        StoredProcedureQuery storedProcedureQuery = entityManager.createStoredProcedureQuery("fn_admin_delete_location_image")
                .registerStoredProcedureParameter("ifileuuid",String.class,ParameterMode.IN)
                .registerStoredProcedureParameter("actioncode",String.class,ParameterMode.OUT)
                .setParameter("ifileuuid",locationUuid);
        String actionCode = (String) storedProcedureQuery.getOutputParameterValue("actioncode");
        entityManager.clear();
        return actionCode;
    }

    @Override
    @Transactional
    public String restoreLocation(String locationUuid) {
        StoredProcedureQuery storedProcedureQuery = entityManager.createStoredProcedureQuery("fn_admin_restore_location")
                .registerStoredProcedureParameter("ilocationuuid",String.class,ParameterMode.IN)
                .registerStoredProcedureParameter("actioncode",String.class,ParameterMode.OUT)
                .setParameter("ilocationuuid",locationUuid);
        String actionCode = (String) storedProcedureQuery.getOutputParameterValue("actioncode");
        entityManager.clear();
        return actionCode;
    }

    @Override
    @Transactional
    public int countLoaction() {
        int countLocation = 0;
        Query storedProcedureQuery = entityManager.createNativeQuery("select count(*) from location where status = '0'");
        countLocation = ((BigInteger) storedProcedureQuery.getSingleResult()).intValue();
        entityManager.clear();
        return countLocation;
    }

    @Override
    public List<LocationSelect> getAllLocation() {
        List<LocationSelect> locationSelectList = new ArrayList<>();
        Query query = entityManager.createNativeQuery("select uuid as uuid,place_name from location where status = '0' order by place_name",LocationSelect.class);
        locationSelectList = (List<LocationSelect>)  query.getResultList();
        return locationSelectList;
    }

    @Override
    public List<LocationImage> getLocationImages(String locationUuid) {
        List<LocationImage> locationImageList = new ArrayList<>();
        StoredProcedureQuery storedProcedureQuery = entityManager.createStoredProcedureQuery("fn_admin_get_location_image",LocationImage.class)
                .registerStoredProcedureParameter("ilocationuuid", String.class, ParameterMode.IN)
                .setParameter("ilocationuuid", locationUuid);
        try {
            locationImageList = storedProcedureQuery.getResultList();
        }catch (Exception ex){
            ex.printStackTrace();
        }
        entityManager.clear();
        return locationImageList;
    }

    @Override
    @Transactional
    public String addLocationTag(Location location) {
        StoredProcedureQuery storedProcedureQuery = entityManager.createStoredProcedureQuery("fn_admin_add_location_tags")
                .registerStoredProcedureParameter("itagnames", String.class, ParameterMode.IN)
                .registerStoredProcedureParameter("ilocationuuid", String.class, ParameterMode.IN)
                .registerStoredProcedureParameter("actioncode", String.class, ParameterMode.OUT )
                .setParameter("itagnames",location.getTagNames())
                .setParameter("ilocationuuid",location.getUuid());
        String actionCode = (String) storedProcedureQuery.getOutputParameterValue("actioncode");
        entityManager.clear();
        return actionCode;
    }
}
