package com.kosign.kmp.repositories.imp;

import com.kosign.kmp.models.Food;
import com.kosign.kmp.repositories.FoodRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.math.BigInteger;
import java.util.List;

@Repository
public class FoodRepositoryImp implements FoodRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Food> getFoodList(int limit,int page) {
        StoredProcedureQuery storedProcedureQuery = entityManager.createStoredProcedureQuery("fn_admin_get_foods",Food.class)
                .registerStoredProcedureParameter("ipage", Integer.class, ParameterMode.IN)
                .registerStoredProcedureParameter("ilimit", Integer.class ,ParameterMode.IN)
                .setParameter("ipage", page)
                .setParameter("ilimit", limit);


        return storedProcedureQuery.getResultList();
    }

    @Override
    @Transactional
    public String insertFood(Food food) {
        System.out.println(food.toString());
        StoredProcedureQuery storedProcedureQuery = entityManager.createStoredProcedureQuery("fn_admin_create_food")
                .registerStoredProcedureParameter("iname",String.class,ParameterMode.IN)
                .registerStoredProcedureParameter("iprice",Double.class,ParameterMode.IN)
                .registerStoredProcedureParameter("iavailableon",String.class,ParameterMode.IN)
                .registerStoredProcedureParameter("ilocationuuid",String.class,ParameterMode.IN)
                .registerStoredProcedureParameter("icategoryuuid",String.class,ParameterMode.IN)
                .registerStoredProcedureParameter("actioncode",String.class,ParameterMode.OUT)
                .setParameter("iname",food.getFoodName())
                .setParameter("iprice",food.getPrice())
                .setParameter("iavailableon",food.getAvailableOn())
                .setParameter("ilocationuuid", food.getPlaceUuid())
                .setParameter("icategoryuuid", food.getCategoryUuid());

        String actionCode = (String) storedProcedureQuery.getOutputParameterValue("actioncode");
        storedProcedureQuery.executeUpdate();
        entityManager.clear();

        return actionCode;
    }

    @Override
    @Transactional
    public String updateFood(Food food) {
        StoredProcedureQuery storedProcedureQuery = entityManager.createStoredProcedureQuery("fn_admin_update_food")
                .registerStoredProcedureParameter("iname",String.class,ParameterMode.IN)
                .registerStoredProcedureParameter("iprice",Double.class,ParameterMode.IN)
                .registerStoredProcedureParameter("iavailableon",String.class,ParameterMode.IN)
                .registerStoredProcedureParameter("ilocationuuid",String.class,ParameterMode.IN)
                .registerStoredProcedureParameter("icategoryuuid",String.class,ParameterMode.IN)
                .registerStoredProcedureParameter("iuuid",String.class,ParameterMode.IN)
                .registerStoredProcedureParameter("actioncode",String.class,ParameterMode.OUT)
                .setParameter("iname",food.getFoodName())
                .setParameter("iprice",food.getPrice())
                .setParameter("iavailableon",food.getAvailableOn())
                .setParameter("ilocationuuid", food.getPlaceUuid())
                .setParameter("icategoryuuid", food.getCategoryUuid())
                .setParameter("iuuid",food.getUuid());

        String actionCode = (String) storedProcedureQuery.getOutputParameterValue("actioncode");
        storedProcedureQuery.executeUpdate();
        entityManager.clear();
        return actionCode;
    }

    @Override
    @Transactional
    public String insertFoodTags(Food food) {
        StoredProcedureQuery storedProcedureQuery = entityManager.createStoredProcedureQuery("fn_admin_add_food_tags")
                .registerStoredProcedureParameter("itagnames", String.class, ParameterMode.IN)
                .registerStoredProcedureParameter("ifooduuid", String.class, ParameterMode.IN)
                .registerStoredProcedureParameter("actioncode", String.class, ParameterMode.OUT )
                .setParameter("itagnames",food.getTagNames())
                .setParameter("ifooduuid",food.getUuid());
        String actionCode = (String) storedProcedureQuery.getOutputParameterValue("actioncode");
        entityManager.clear();
        return actionCode;
    }

    @Override
    @Transactional
    public String insertFoodPicture(Food food) {
        StoredProcedureQuery storedProcedureQuery = entityManager.createStoredProcedureQuery("fn_admin_add_food_pictures")
                .registerStoredProcedureParameter("ipictures", String.class, ParameterMode.IN)
                .registerStoredProcedureParameter("ifooduuid", String.class, ParameterMode.IN)
                .registerStoredProcedureParameter("actioncode", String.class, ParameterMode.OUT)
                .setParameter("ipictures", food.getFileName())
                .setParameter("ifooduuid", food.getUuid());
        String actionCode = (String) storedProcedureQuery.getOutputParameterValue("actioncode");
        entityManager.clear();
        return actionCode;
    }

    @Override
    public int countFoods() {
        int countLocation = 0;
        Query storedProcedureQuery = entityManager.createNativeQuery("select count(*) from food where status = '0' and event_id is null ");
        countLocation = ((BigInteger) storedProcedureQuery.getSingleResult()).intValue();
        entityManager.clear();
        return countLocation;
    }

    @Override
    @Transactional
    public String toggleFoodStatus(String uuid) {
        StoredProcedureQuery storedProcedureQuery = entityManager.createStoredProcedureQuery("fn_admin_toggle_food_status")
                .registerStoredProcedureParameter("iuuid", String.class, ParameterMode.IN)
                .registerStoredProcedureParameter("actioncode", String.class, ParameterMode.OUT)
                .setParameter("iuuid", uuid);
        String actionCode = (String) storedProcedureQuery.getOutputParameterValue("actioncode");
        entityManager.clear();
        return actionCode;
    }
}
