package com.kosign.kmp.repositories.imp;

import com.kosign.kmp.models.Category;
import com.kosign.kmp.models.MainCategory;
import com.kosign.kmp.repositories.CategoryRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
@Repository
public class CategoryRepositoryImp implements CategoryRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Category> getCategoryList(int limit, int page) {
        List<Category> categoryList = new ArrayList<>();
        StoredProcedureQuery storedProcedureQuery = entityManager.createStoredProcedureQuery("fn_admin_get_categories",Category.class)
                .registerStoredProcedureParameter("ipage",Integer.class,ParameterMode.IN)
                .registerStoredProcedureParameter("ilimit",Integer.class,ParameterMode.IN)
                .setParameter("ipage",page)
                .setParameter("ilimit",limit);

        try {
            categoryList = storedProcedureQuery.getResultList();

        }catch (Exception ex){
            ex.printStackTrace();
        }
        entityManager.clear();
        return categoryList;
    }

    @Override
    public List<Category> getAllSubcategories() {
        List<Category> categoryList = new ArrayList<>();
        StoredProcedureQuery storedProcedureQuery = entityManager.createStoredProcedureQuery("fn_admin_get_categories",Category.class);
        try {
            categoryList = storedProcedureQuery.getResultList();
            System.out.println("count:"+categoryList.size());
        }catch (Exception ex){
            ex.printStackTrace();
        }
        entityManager.clear();
        return categoryList;
    }

    @Override
    public List<MainCategory> getAllMaincategories() {
        List<MainCategory> mainCategoryList = new ArrayList<>();
        Query query = entityManager.createNativeQuery("select uuid,category_name from category where category_id is null order by category_name", MainCategory.class);
        mainCategoryList = query.getResultList();
        return mainCategoryList;
    }

    @Override
    @Transactional
    public String insertCategory(Category category) {
        StoredProcedureQuery storedProcedureQuery = entityManager.createStoredProcedureQuery("fn_admin_create_category")
                .registerStoredProcedureParameter("iname", String.class, ParameterMode.IN)
                .registerStoredProcedureParameter("ipicture", String.class, ParameterMode.IN)
                .registerStoredProcedureParameter("iparentuuid", String.class, ParameterMode.IN)
                .registerStoredProcedureParameter("actioncode", String.class, ParameterMode.OUT)
                .setParameter("iname", category.getCategoryName())
                .setParameter("ipicture", category.getCategoryPicture())
                .setParameter("iparentuuid",category.getCategoryUuid());
        String actionCode = (String) storedProcedureQuery.getOutputParameterValue("actioncode");
        entityManager.clear();
        return actionCode;
    }

    @Override
    @Transactional
    public String updateCategory(Category category) {

        StoredProcedureQuery storedProcedureQuery = entityManager.createStoredProcedureQuery("fn_admin_update_category")
                .registerStoredProcedureParameter("icategoryname", String.class, ParameterMode.IN)
                .registerStoredProcedureParameter("icategorypicture", String.class, ParameterMode.IN)
                .registerStoredProcedureParameter("iuuid", String.class, ParameterMode.IN)
                .registerStoredProcedureParameter("actioncode", String.class, ParameterMode.OUT)
                .setParameter("icategoryname", category.getCategoryName())
                .setParameter("icategorypicture", category.getCategoryPicture())
                .setParameter("iuuid", category.getSubcategoryUuid());
        String actionCode = (String) storedProcedureQuery.getOutputParameterValue("actioncode");
        entityManager.clear();
        return actionCode;
    }

    @Override
    @Transactional
    public String toggleCategoryStatus(String categoryUuid) {
        StoredProcedureQuery storedProcedureQuery = entityManager.createStoredProcedureQuery("fn_admin_toggle_category_status")
                .registerStoredProcedureParameter("iuuid", String.class, ParameterMode.IN)
                .registerStoredProcedureParameter("actioncode", String.class, ParameterMode.OUT)
                .setParameter("iuuid",categoryUuid);
        String actionCode = (String) storedProcedureQuery.getOutputParameterValue("actioncode");
        entityManager.clear();
        return actionCode;
    }

    @Override
    @Transactional
    public int count() {
        int countLocation = 0;
        Query storedProcedureQuery = entityManager.createNativeQuery("select count(*) from category where status = '0' and category_id is not null");
        countLocation = ((BigInteger) storedProcedureQuery.getSingleResult()).intValue();
        entityManager.clear();
        return countLocation;
    }



    @Override
    @Transactional
    public String moveCategory(String mainCategoryUuid, String subcategoryUuid) {
        StoredProcedureQuery storedProcedureQuery = entityManager.createStoredProcedureQuery("fn_admin_move_category")
                .registerStoredProcedureParameter("imaincategoryuuid", String.class, ParameterMode.IN)
                .registerStoredProcedureParameter("isubcategoryuuid", String.class, ParameterMode.IN)
                .registerStoredProcedureParameter("actioncode", String.class, ParameterMode.OUT)
                .setParameter("imaincategoryuuid", mainCategoryUuid)
                .setParameter("isubcategoryuuid", subcategoryUuid);
        String actionCode = (String) storedProcedureQuery.getOutputParameterValue("actioncode");
        entityManager.clear();
        return actionCode;
    }

}
