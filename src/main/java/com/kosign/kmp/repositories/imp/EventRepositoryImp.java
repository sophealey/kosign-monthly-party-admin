package com.kosign.kmp.repositories.imp;

import com.kosign.kmp.models.Event;
import com.kosign.kmp.repositories.EventRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@Repository
public class EventRepositoryImp implements EventRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Event> getEventList(String lmsDepartmentUUId, int limit, int page) {

        List<Event> eventList = new ArrayList<>();

        StoredProcedureQuery storedProcedureQuery = entityManager.createStoredProcedureQuery("fn_admin_get_event",Event.class)
                .registerStoredProcedureParameter("ilmsdepartmentuuid", String.class, ParameterMode.IN)
                .registerStoredProcedureParameter("ipage", Integer.class, ParameterMode.IN)
                .registerStoredProcedureParameter("ilimit", Integer.class, ParameterMode.IN)
                .setParameter("ilmsdepartmentuuid",lmsDepartmentUUId)
                .setParameter("ipage",page)
                .setParameter("ilimit",limit);

        eventList = storedProcedureQuery.getResultList();
        return eventList;
    }

    @Override
    @Transactional
    public int count(String dept_uuid) {
        int countLocation = 0;
        Query storedProcedureQuery = entityManager.createNativeQuery("SELECT COUNT(*) FROM event e INNER JOIN department d ON d.id = e.department_id INNER JOIN location l ON e.location_id = l.id LEFT JOIN \"join\" j ON e.id = j.event_id WHERE d.uuid = '" + dept_uuid + "'");
        countLocation = ((BigInteger) storedProcedureQuery.getSingleResult()).intValue();
        entityManager.clear();
        return countLocation;
    }

    @Override
    public int countAllEvent() {
        int countLocation = 0;
        Query storedProcedureQuery = entityManager.createNativeQuery("SELECT COUNT(*) FROM event");
        countLocation = ((BigInteger) storedProcedureQuery.getSingleResult()).intValue();
        entityManager.clear();
        return countLocation;
    }

    @Override
    @Transactional
    public String toggleEventStatus(String uuid) {
        StoredProcedureQuery storedProcedureQuery = entityManager.createStoredProcedureQuery("fn_admin_toggle_event_status")
                .registerStoredProcedureParameter("ieventuuid", String.class, ParameterMode.IN)
                .registerStoredProcedureParameter("actioncode", String.class, ParameterMode.OUT)
                .setParameter("ieventuuid",uuid);
        String actionCode = (String) storedProcedureQuery.getOutputParameterValue("actioncode");
        entityManager.clear();
        return actionCode;
    }

    @Override
    public int countDepartment() {
        int countLocation = 0;
        Query storedProcedureQuery = entityManager.createNativeQuery("SELECT COUNT(*) FROM department");
        countLocation = ((BigInteger) storedProcedureQuery.getSingleResult()).intValue();
        entityManager.clear();
        return countLocation;
    }
}
