package com.kosign.kmp.controllers;


import com.kosign.kmp.models.Location;
import com.kosign.kmp.services.CategoryService;
import com.kosign.kmp.services.EventService;
import com.kosign.kmp.services.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/admin")
public class HomeController {

    @Autowired
    private LocationService locationService;
    @Autowired
    private EventService eventService;
    @Autowired
    private CategoryService categoryService;

    @RequestMapping("/index")
    public String home(){
        List<Location> locationList = new ArrayList<>();
         locationList = locationService.getLocatoinList(10,1);
        System.out.println(locationList.size());
        return "index";
    }

    @RequestMapping("/location_lists")
    public List<Location> getLocationList(){
        List<Location> locationList = new ArrayList<>();
        locationList.forEach(location -> {
            System.out.println(location.getLocationName());
        });
        return locationList;
    }

    @GetMapping("/ajax/get_all_count")
    @ResponseBody
    public Map<String,Object> getAllCount(){
        Map<String,Object> response = new HashMap<>();
        try {
            response.put("status", true);
            response.put("cout_category",categoryService.count());
            response.put("count_department",eventService.countDepartment());
            response.put("count_location",locationService.countLocatoin());
            response.put("count_event",eventService.countAllEvent());

        }catch (Exception ex){
            response.put("status",false);
            ex.printStackTrace();
        }
        System.out.println(response);
        return response;
    }


}
