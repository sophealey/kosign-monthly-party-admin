package com.kosign.kmp.controllers;

import com.kosign.kmp.models.Location;
import com.kosign.kmp.models.LocationImage;
import com.kosign.kmp.models.LocationSelect;
import com.kosign.kmp.services.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/admin")
public class LocationController {
    @Autowired
    private LocationService locationService;

    @RequestMapping("/location_list")
    public String gotoLocationList(){
        return "location";
    }

    @PostMapping("/ajax/get_location_list")
    @ResponseBody
    public Map<String,Object> getLocationList(@ModelAttribute("limit") int limit,@ModelAttribute("page") int page){

        Map<String,Object> response = new HashMap<>();
        List<Location> locationList = locationService.getLocatoinList(limit,page);
        int countLocation = locationService.countLocatoin();

        response.put("status", !locationList.isEmpty());
        response.put("data", locationList);
        response.put("count", countLocation);

        return response;
    }

    @PostMapping("/ajax/insert_location")
    @ResponseBody
    public Map<String,Object> insertLocation(@RequestBody Location location){
        Map<String,Object> response = new HashMap<>();
        String actionCode = locationService.insertLocation(location);
        if (actionCode.equals("00000")){
            response.put("status",true);
        }else {
            response.put("status",false);
        }

        return response;
    }

    @PostMapping("/ajax/update_location")
    @ResponseBody
    public Map<String,Object> updateLocation(@RequestBody Location location){
        Map<String,Object> response = new HashMap<>();
        String actionCode = locationService.updateLocation(location);
        if (actionCode.equals("00000")){
            response.put("status",true);
        }else {
            response.put("status",false);
        }

        return response;
    }

    @PostMapping("/ajax/delete_location")
    @ResponseBody
    public Map<String,Object> deleteLocation(@RequestParam("uuid") String uuid){
        Map<String,Object> response = new HashMap<>();
        String actionCode = locationService.deleteLocation(uuid);
        if (actionCode.equals("00000")){
            response.put("status",true);
        }else {
            response.put("status",false);
        }

        return response;
    }

    @PostMapping("/ajax/delete_location_image")
    @ResponseBody
    public Map<String,Object> deleteLocationImage(@RequestParam("uuid") String uuid){
        Map<String,Object> response = new HashMap<>();
        String actionCode = locationService.deleteLocationImage(uuid);
        if (actionCode.equals("00000")){
            response.put("status",true);
        }else {
            response.put("status",false);
        }

        return response;
    }

    @PostMapping("/ajax/restore_location")
    @ResponseBody
    public Map<String,Object> restoreLocation(@RequestParam("uuid") String uuid){
        Map<String,Object> response = new HashMap<>();
        String actionCode = locationService.restoreLocation(uuid);
        if (actionCode.equals("00000")){
            response.put("status",true);
        }else {
            response.put("status",false);
        }

        return response;
    }

    @GetMapping("/ajax/get_location_select")
    @ResponseBody
    public Map<String,Object> getAllLocation(){
        Map<String,Object> response = new HashMap<>();
        List<LocationSelect> locationSelectList = new ArrayList<>();
        locationSelectList = locationService.getAllLocation();
            response.put("status",!locationSelectList.isEmpty());
            response.put("data",locationSelectList);
        return response;
    }

    @PostMapping("/ajax/get_location_images")
    @ResponseBody
    public Map<String, Object> getLocationImage(@RequestParam("uuid") String locationUuid){
        Map<String,Object> response = new HashMap<>();
        List<LocationImage> locationImageList = new ArrayList<>();
        locationImageList = locationService.getLocationImages(locationUuid);
        response.put("status", !locationImageList.isEmpty());
        response.put("data", locationImageList);
        return response;
    }

    @PostMapping("/ajax/insert_location_tag")
    @ResponseBody
    Map<String,Object> insertFoodTag(@RequestBody Location location){
        Map<String,Object> response = new HashMap<>();
        try {
            String actionCode = locationService.addLocationTag(location);
            if (actionCode.equals("00000")){
                response.put("status",true);
            }else {
                response.put("status",false);
            }
        }catch (Exception ex){
            response.put("status",false);
            ex.printStackTrace();
        }

        return response;
    }
}
