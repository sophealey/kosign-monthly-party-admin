package com.kosign.kmp.controllers;

import com.kosign.kmp.models.Food;
import com.kosign.kmp.services.FoodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/admin")
public class FoodController {

    @Autowired
    private FoodService foodService;

    @RequestMapping("/food_list")
    public String gotoFoodList(){
        return "foods";
    }

    @GetMapping("/ajax/get_food_list")
    @ResponseBody
    Map<String,Object> getFoodList(@ModelAttribute("limit") int limit, @ModelAttribute("page") int page){
        Map<String,Object> response = new HashMap<>();
        List<Food> foodList = new ArrayList<>();
        int foodCount = 0;
        try {
            foodList = foodService.getFoodList(limit, page);
            foodCount = foodService.countFoods();

            response.put("status",!foodList.isEmpty());
            response.put("data", foodList);
            response.put("count",foodCount);
        }catch (Exception ex) {
            response.put("status",false);
            ex.printStackTrace();
        }
        return response;
    }


    @PostMapping("/ajax/insert_food")
    @ResponseBody
    Map<String,Object> insertFood(@RequestBody Food food){
        Map<String,Object> response = new HashMap<>();
        try {
            String actionCode = foodService.insertFood(food);
            if (actionCode.equals("00000")){
                response.put("status",true);
            }else {
                response.put("status",false);
            }
        }catch (Exception ex){
            response.put("status",false);
            ex.printStackTrace();
        }

        return response;
    }

    @PostMapping("/ajax/update_food")
    @ResponseBody
    Map<String,Object> updateFood(@RequestBody Food food){
        Map<String,Object> response = new HashMap<>();
        try {
            String actionCode = foodService.updateFood(food);
            if (actionCode.equals("00000")){
                response.put("status",true);
            }else {
                response.put("status",false);
            }
        }catch (Exception ex){
            response.put("status",false);
            ex.printStackTrace();
        }

        return response;
    }

    @PostMapping("/ajax/insert_foodtag")
    @ResponseBody
    Map<String,Object> insertFoodTag(@RequestBody Food food){
        Map<String,Object> response = new HashMap<>();
        try {
            String actionCode = foodService.insertFoodTags(food);
            if (actionCode.equals("00000")){
                response.put("status",true);
            }else {
                response.put("status",false);
            }
        }catch (Exception ex){
            response.put("status",false);
            ex.printStackTrace();
        }

        return response;
    }

    @PostMapping("/ajax/insert_foodpicture")
    @ResponseBody
    Map<String,Object> insertFoodPicture(@RequestBody Food food){
        Map<String,Object> response = new HashMap<>();
        try {
            String actionCode = foodService.insertFoodPicture(food);
            if (actionCode.equals("00000")){
                response.put("status",true);
            }else {
                response.put("status",false);
            }
        }catch (Exception ex){
            response.put("status",false);
            ex.printStackTrace();
        }

        return response;
    }

    @PostMapping("/ajax/toggle_food_status")
    @ResponseBody
    Map<String, Object> toggleFoodStatus(@RequestParam("uuid") String uuid ){
        Map<String, Object> response = new HashMap<>();
        try {
            String actionCode = foodService.toggleFoodStatus(uuid);
            if (actionCode.equals("00000")){
                response.put("status", true);
            }else {
                response.put("status", false);
            }
        }catch (Exception ex) {
            response.put("status", false);
            ex.printStackTrace();
        }
        return response;
    }
}
