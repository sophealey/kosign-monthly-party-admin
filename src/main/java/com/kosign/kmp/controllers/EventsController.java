package com.kosign.kmp.controllers;

import com.kosign.kmp.models.Event;
import com.kosign.kmp.services.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/admin")
public class EventsController {
    @Autowired
    private EventService eventService;

    @RequestMapping("/event_list")
    public String gotoLocationList(){
        return "event";
    }

    @PostMapping("/ajax/get_event_list")
    @ResponseBody
    public Map<String,Object> getEventList(@ModelAttribute("dept_uuid") String lmsDepartmentUUId, @ModelAttribute("limit") int limit, @ModelAttribute("page") int page){

        Map<String,Object> response = new HashMap<>();
        List<Event> eventList = eventService.getEventList(lmsDepartmentUUId, limit, page);

        response.put("status", !eventList.isEmpty());
        response.put("count", eventService.count(lmsDepartmentUUId));
        response.put("data", eventList);

        return response;
    }

    @PostMapping("/ajax/delete_event")
    @ResponseBody
    public Map<String,Object> deleteLocation(@RequestParam("uuid") String uuid){
        Map<String,Object> response = new HashMap<>();
        String actionCode = eventService.toggleEventStatus(uuid);
        if (actionCode.equals("00000")){
            response.put("status",true);
        }else {
            response.put("status",false);
        }

        return response;
    }

}
