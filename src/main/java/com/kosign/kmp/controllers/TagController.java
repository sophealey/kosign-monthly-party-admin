package com.kosign.kmp.controllers;

import com.kosign.kmp.models.Category;
import com.kosign.kmp.models.Tag;
import com.kosign.kmp.services.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/admin")
public class TagController {

    @Autowired
    private TagService tagService;

    @GetMapping("/tag_list")
    public String gotoTagList(){
        return "tag";
    }

    @GetMapping("/ajax/get_tag_list")
    @ResponseBody
    public Map<String,Object> getTagList(int limit, int page){
        Map<String,Object> response = new HashMap<>();
        List<Tag> tagList = new ArrayList<>();
        tagList = tagService.getTagList(limit, page);

        response.put("status",!tagList.isEmpty());
        response.put("count", tagService.count());
        response.put("data",tagList);

        return response;
    }

    @PostMapping("/ajax/update_tag")
    @ResponseBody
    public Map<String,Object> updateTag(@RequestBody Tag tag){
        Map<String,Object> response = new HashMap<>();
        String actionCode = tagService.updateTage(tag);
        if (actionCode.equals("00000")){
            response.put("status",true);
        }else {
            response.put("status",false);
        }

        return response;
    }

    @PostMapping("/ajax/delete_tag")
    @ResponseBody
    public Map<String,Object> deleteTag(@RequestParam String uuid){
        Map<String,Object> response = new HashMap<>();
        String actionCode = tagService.toggleTagStatus(uuid);
        if (actionCode.equals("00000")){
            response.put("status",true);
        }else {
            response.put("status",false);
        }

        return response;
    }
}
