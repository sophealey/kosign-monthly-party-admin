package com.kosign.kmp.controllers;

import com.kosign.kmp.models.Category;
import com.kosign.kmp.models.MainCategory;
import com.kosign.kmp.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/admin")
public class CategoryController {
    @Autowired
    private CategoryService categoryService;

    @RequestMapping("/category_list")
    public String gotoLocationList(){
        return "category";
    }

    @PostMapping("/ajax/get_category_list")
    @ResponseBody
    public Map<String,Object> getLocationList(@ModelAttribute("limit") int limit,@ModelAttribute("page") int page){

        Map<String,Object> response = new HashMap<>();
        List<Category> categoryList = categoryService.getCategoryList(limit, page);
        int countCategory = categoryService.count();

        response.put("status", !categoryList.isEmpty());
        response.put("data", categoryList);
        response.put("count", countCategory);

        return response;
    }


    @PostMapping("/ajax/insert_category")
    @ResponseBody
    public Map<String,Object> insertCategory(@RequestBody Category category){
        Map<String,Object> response = new HashMap<>();
        String actionCode = categoryService.insertCategory(category);
        if (actionCode.equals("00000")){
            response.put("status",true);
        }else {
            response.put("status",false);
        }

        return response;
    }

    @PostMapping("/ajax/update_category")
    @ResponseBody
    public Map<String,Object> updateCategory(@RequestBody Category category){
        Map<String,Object> response = new HashMap<>();
        String actionCode = categoryService.updateCategory(category);
        if (actionCode.equals("00000")){
            response.put("status",true);
        }else {
            response.put("status",false);
        }

        return response;
    }

    @PostMapping("/ajax/delete_category")
    @ResponseBody
    public Map<String,Object> deleteLocation(@RequestParam String uuid){
        Map<String,Object> response = new HashMap<>();
        String actionCode = categoryService.toggleCategoryStatus(uuid);
        if (actionCode.equals("00000")){
            response.put("status",true);
        }else {
            response.put("status",false);
        }

        return response;
    }

    @GetMapping("/ajax/get_all_subcategory")
    @ResponseBody
    public Map<String,Object> getAllSubcategories(){

        Map<String,Object> response = new HashMap<>();
        List<Category> categoryList = categoryService.getAllSubcategories();
        int countCategory = categoryService.count();

        response.put("status", !categoryList.isEmpty());
        response.put("data", categoryList);

        return response;
    }

    @GetMapping("/ajax/get_all_maincategory")
    @ResponseBody
    public Map<String,Object> getAllMaincategories(){

        Map<String,Object> response = new HashMap<>();
        List<MainCategory> mainCategoryList = categoryService.getAllMaincategories();
        int countCategory = categoryService.count();

        response.put("status", !mainCategoryList.isEmpty());
        response.put("data", mainCategoryList);

        return response;
    }

    @PostMapping("/ajax/move_subcategory")
    @ResponseBody
    public Map<String,Object> moveSubcategory(@RequestParam String mainCategoryUuid, @RequestParam String subcategoryUuid){
        Map<String,Object> response = new HashMap<>();
        String actionCode = categoryService.moveCategory(mainCategoryUuid, subcategoryUuid);
        if (actionCode.equals("00000")){
            response.put("status",true);
        }else {
            response.put("status",false);
        }
        return response;
    }


}
