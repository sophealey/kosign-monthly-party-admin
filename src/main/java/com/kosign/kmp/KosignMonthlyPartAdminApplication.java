package com.kosign.kmp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
public class KosignMonthlyPartAdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(KosignMonthlyPartAdminApplication.class, args);
    }

}

