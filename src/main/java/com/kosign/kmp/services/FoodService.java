package com.kosign.kmp.services;

import com.kosign.kmp.models.Food;

import java.util.List;

public interface FoodService {
    List<Food> getFoodList(int limit, int page);
    String insertFood(Food food);
    String updateFood(Food food);
    String insertFoodTags(Food food);
    String insertFoodPicture(Food food);
    int countFoods();
    String toggleFoodStatus(String uuid);
}
