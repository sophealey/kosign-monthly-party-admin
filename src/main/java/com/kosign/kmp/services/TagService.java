package com.kosign.kmp.services;

import com.kosign.kmp.models.Tag;

import java.util.List;

public interface TagService {
    List<Tag> getTagList(int limit, int page);
    String updateTage(Tag tag);
    String toggleTagStatus(String uuid);
    int count();
}
