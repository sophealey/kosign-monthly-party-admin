package com.kosign.kmp.services;

import com.kosign.kmp.models.Location;
import com.kosign.kmp.models.LocationImage;
import com.kosign.kmp.models.LocationSelect;

import java.util.List;

public interface LocationService {
    List<Location> getLocatoinList(int limit, int page);
    String insertLocation(Location location);
    String updateLocation(Location location);
    String deleteLocation(String uuid);
    String deleteLocationImage(String locationUuid);
    String restoreLocation(String uuid);
    int countLocatoin();
    List<LocationSelect> getAllLocation();
    List<LocationImage> getLocationImages(String locationUuid);
    String addLocationTag(Location location);
}
