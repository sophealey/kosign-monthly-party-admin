package com.kosign.kmp.services.imp;

import com.kosign.kmp.models.Location;
import com.kosign.kmp.models.LocationImage;
import com.kosign.kmp.models.LocationSelect;
import com.kosign.kmp.repositories.LocationRepository;
import com.kosign.kmp.services.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LocationServiceImp implements LocationService {

    @Autowired
    private LocationRepository locationRepository;
    @Override
    public List<Location> getLocatoinList(int limit, int page) {
        return locationRepository.getLocatoinList(limit,page);
    }

    @Override
    public String insertLocation(Location location) {
        return locationRepository.insertLocation(location);
    }

    @Override
    public String updateLocation(Location location) {
        return locationRepository.updateLocation(location);
    }

    @Override
    public String deleteLocation(String uuid) {
        return locationRepository.deleteLocation(uuid);
    }

    @Override
    public String deleteLocationImage(String locationUuid) {
        return locationRepository.deleteLocationImage(locationUuid);
    }

    @Override
    public String restoreLocation(String uuid) {
        return locationRepository.restoreLocation(uuid);
    }


    @Override
    public int countLocatoin() {
        return locationRepository.countLoaction();
    }

    @Override
    public List<LocationSelect> getAllLocation() {
        return locationRepository.getAllLocation();
    }

    @Override
    public List<LocationImage> getLocationImages(String locationUuid) {
        return locationRepository.getLocationImages(locationUuid);
    }

    @Override
    public String addLocationTag(Location location) {
        return locationRepository.addLocationTag(location);
    }
}
