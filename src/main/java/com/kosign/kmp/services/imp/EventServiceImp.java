package com.kosign.kmp.services.imp;

import com.kosign.kmp.models.Event;
import com.kosign.kmp.repositories.EventRepository;
import com.kosign.kmp.services.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class EventServiceImp implements EventService {

    @Autowired
    private EventRepository eventRepository;

    @Override
    public List<Event> getEventList(String lmsDepartmentUUId, int limit, int page) {
        return eventRepository.getEventList(lmsDepartmentUUId,limit,page);
    }

    @Override
    public int count(String dept_uuid) {
        return eventRepository.count(dept_uuid);
    }

    @Override
    public int countAllEvent() {
        return eventRepository.countAllEvent();
    }

    @Override
    public String toggleEventStatus(String uuid) {
        return eventRepository.toggleEventStatus(uuid);
    }

    @Override
    public int countDepartment() {
        return eventRepository.countDepartment();
    }
}
