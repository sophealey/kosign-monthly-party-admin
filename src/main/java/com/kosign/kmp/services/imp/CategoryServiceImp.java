package com.kosign.kmp.services.imp;

import com.kosign.kmp.models.Category;
import com.kosign.kmp.models.MainCategory;
import com.kosign.kmp.repositories.CategoryRepository;
import com.kosign.kmp.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CategoryServiceImp implements CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public List<Category> getCategoryList(int limit, int page) {
        return categoryRepository.getCategoryList(limit, page);
    }

    @Override
    public List<Category> getAllSubcategories() {
        return categoryRepository.getAllSubcategories();
    }

    @Override
    public List<MainCategory> getAllMaincategories() {
        return categoryRepository.getAllMaincategories();
    }

    @Override
    public String insertCategory(Category category) {
        return categoryRepository.insertCategory(category);
    }

    @Override
    public String updateCategory(Category category) {
        return categoryRepository.updateCategory(category);
    }

    @Override
    public String toggleCategoryStatus(String categoryUuid) {
        return categoryRepository.toggleCategoryStatus(categoryUuid);
    }

    @Override
    public int count() {
        return categoryRepository.count();
    }

    @Override
    public String moveCategory(String mainCategoryUuid, String subcategoryUuid) {
        return categoryRepository.moveCategory(mainCategoryUuid, subcategoryUuid);
    }
}
