package com.kosign.kmp.services.imp;

import com.kosign.kmp.models.Tag;
import com.kosign.kmp.repositories.TagRepository;
import com.kosign.kmp.services.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TagServiceImp implements TagService {

    @Autowired
    private TagRepository tagRepository;

    @Override
    public List<Tag> getTagList(int limit, int page) {
        return tagRepository.getTagList(limit, page);
    }

    @Override
    public String updateTage(Tag tag) {
        return tagRepository.updateTage(tag);
    }

    @Override
    public String toggleTagStatus(String uuid) {
        return tagRepository.toggleTagStatus(uuid);
    }

    @Override
    public int count() {
        return tagRepository.count();
    }
}
