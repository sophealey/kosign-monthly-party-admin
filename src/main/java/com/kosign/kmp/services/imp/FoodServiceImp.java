package com.kosign.kmp.services.imp;

import com.kosign.kmp.models.Food;
import com.kosign.kmp.repositories.FoodRepository;
import com.kosign.kmp.services.FoodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FoodServiceImp implements FoodService {

    @Autowired
    private FoodRepository foodRepository;

    @Override
    public List<Food> getFoodList(int limit,int page) {
        return foodRepository.getFoodList(limit,page);
    }

    @Override
    public String insertFood(Food food) {
        return foodRepository.insertFood(food);
    }

    @Override
    public String updateFood(Food food) {
        return foodRepository.updateFood(food);
    }

    @Override
    public String insertFoodTags(Food food) {
        return foodRepository.insertFoodTags(food);
    }

    @Override
    public String insertFoodPicture(Food food) {
        return foodRepository.insertFoodPicture(food);
    }

    @Override
    public int countFoods() {
        return foodRepository.countFoods();
    }

    @Override
    public String toggleFoodStatus(String uuid) {
        return foodRepository.toggleFoodStatus(uuid);
    }


}
