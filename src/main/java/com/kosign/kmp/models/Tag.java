package com.kosign.kmp.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigInteger;
import java.sql.Timestamp;

@Entity
public class Tag {
    @Id
    private int id;
    private String tagName;
    private Timestamp createdDate;
    private String uuid;
    private String status;
    private BigInteger foodCount;
    private BigInteger locationCount;

    public Tag() {
    }

    public Tag(int id, String tagName, Timestamp createdDate, String uuid, String status, BigInteger foodCount, BigInteger locationCount) {
        this.id = id;
        this.tagName = tagName;
        this.createdDate = createdDate;
        this.uuid = uuid;
        this.status = status;
        this.foodCount = foodCount;
        this.locationCount = locationCount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BigInteger getFoodCount() {
        return foodCount;
    }

    public void setFoodCount(BigInteger foodCount) {
        this.foodCount = foodCount;
    }

    public BigInteger getLocationCount() {
        return locationCount;
    }

    public void setLocationCount(BigInteger locationCount) {
        this.locationCount = locationCount;
    }

    @Override
    public String toString() {
        return "Tag{" +
                "id=" + id +
                ", tagName='" + tagName + '\'' +
                ", createdDate=" + createdDate +
                ", uuid='" + uuid + '\'' +
                ", status='" + status + '\'' +
                ", foodCount=" + foodCount +
                ", locationCount=" + locationCount +
                '}';
    }
}
