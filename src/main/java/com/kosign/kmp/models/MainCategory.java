package com.kosign.kmp.models;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class MainCategory {
    @Id
    private String uuid;
    private String categoryName;

    public MainCategory() {
    }

    public MainCategory(String uuid, String categoryName) {
        this.uuid = uuid;
        this.categoryName = categoryName;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    @Override
    public String toString() {
        return "MainCategory{" +
                "uuid='" + uuid + '\'' +
                ", categoryName='" + categoryName + '\'' +
                '}';
    }
}
