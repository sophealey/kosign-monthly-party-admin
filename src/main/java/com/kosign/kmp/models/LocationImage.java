package com.kosign.kmp.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Timestamp;

@Entity
public class LocationImage {

    private String fileName;
    private Timestamp createdDate;
    @Id
    private String fileUuid;

    public LocationImage() {
    }

    public LocationImage(String fileName, Timestamp createdDate, String fileUuid) {
        this.fileName = fileName;
        this.createdDate = createdDate;
        this.fileUuid = fileUuid;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public String getFileUuid() {
        return fileUuid;
    }

    public void setFileUuid(String fileUuid) {
        this.fileUuid = fileUuid;
    }
}
