package com.kosign.kmp.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Timestamp;

@Entity
public class Event {
    private String placeName;
    private String address;
    private Timestamp eventDate;
    private String mediaName;
    private int join;
    private String departmentName;
    @Id
    private String eventUuid;
    private String status;

    public Event() {}

    public Event(String placeName, String address, Timestamp eventDate, String mediaName, int join, String departmentName, String eventUuid, String status) {
        this.placeName = placeName;
        this.address = address;
        this.eventDate = eventDate;
        this.mediaName = mediaName;
        this.join = join;
        this.departmentName = departmentName;
        this.eventUuid = eventUuid;
        this.status = status;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Timestamp getEventDate() {
        return eventDate;
    }

    public void setEventDate(Timestamp eventDate) {
        this.eventDate = eventDate;
    }

    public String getMediaName() {
        return mediaName;
    }

    public void setMediaName(String mediaName) {
        this.mediaName = mediaName;
    }

    public int getJoin() {
        return join;
    }

    public void setJoin(int join) {
        this.join = join;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getEventUuid() {
        return eventUuid;
    }

    public void setEventUuid(String eventUuid) {
        this.eventUuid = eventUuid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
