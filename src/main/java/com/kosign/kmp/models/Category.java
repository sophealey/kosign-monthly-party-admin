package com.kosign.kmp.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.beans.factory.annotation.Value;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.Null;
import java.sql.Timestamp;

@Entity
public class Category {
    private int categoryId;
    private String categoryName;
    private String categoryPicture;
    private Timestamp categoryCreatedDate;
    private String categoryUuid;
    private String categoryStatus;

    private int subcategoryId;
    private String subcategoryName;
    private String subcategoryPicture;
    private Timestamp subcategoryCreatedDate;
    @Id
    private String subcategoryUuid;
    private String subcategoryStatus;

    public Category() {
    }

    public Category(int categoryId, String categoryName, String categoryPicture, Timestamp categoryCreatedDate, String categoryUuid, String categoryStatus, int subcategoryId, String subcategoryName, String subcategoryPicture, Timestamp subcategoryCreatedDate, String subcategoryUuid, String subcategoryStatus) {
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.categoryPicture = categoryPicture;
        this.categoryCreatedDate = categoryCreatedDate;
        this.categoryUuid = categoryUuid;
        this.categoryStatus = categoryStatus;
        this.subcategoryId = subcategoryId;
        this.subcategoryName = subcategoryName;
        this.subcategoryPicture = subcategoryPicture;
        this.subcategoryCreatedDate = subcategoryCreatedDate;
        this.subcategoryUuid = subcategoryUuid;
        this.subcategoryStatus = subcategoryStatus;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryPicture() {
        return categoryPicture;
    }

    public void setCategoryPicture(String categoryPicture) {
        this.categoryPicture = categoryPicture;
    }

    public Timestamp getCategoryCreatedDate() {
        return categoryCreatedDate;
    }

    public void setCategoryCreatedDate(Timestamp categoryCreatedDate) {
        this.categoryCreatedDate = categoryCreatedDate;
    }

    public String getCategoryUuid() {
        return categoryUuid;
    }

    public void setCategoryUuid(String categoryUuid) {
        this.categoryUuid = categoryUuid;
    }

    public String getCategoryStatus() {
        return categoryStatus;
    }

    public void setCategoryStatus(String categoryStatus) {
        this.categoryStatus = categoryStatus;
    }

    public int getSubcategoryId() {
        return subcategoryId;
    }

    public void setSubcategoryId(int subcategoryId) {
        this.subcategoryId = subcategoryId;
    }

    public String getSubcategoryName() {
        return subcategoryName;
    }

    public void setSubcategoryName(String subcategoryName) {
        this.subcategoryName = subcategoryName;
    }

    public String getSubcategoryPicture() {
        return subcategoryPicture;
    }

    public void setSubcategoryPicture(String subcategoryPicture) {
        this.subcategoryPicture = subcategoryPicture;
    }

    public Timestamp getSubcategoryCreatedDate() {
        return subcategoryCreatedDate;
    }

    public void setSubcategoryCreatedDate(Timestamp subcategoryCreatedDate) {
        this.subcategoryCreatedDate = subcategoryCreatedDate;
    }

    public String getSubcategoryUuid() {
        return subcategoryUuid;
    }

    public void setSubcategoryUuid(String subcategoryUuid) {
        this.subcategoryUuid = subcategoryUuid;
    }

    public String getSubcategoryStatus() {
        return subcategoryStatus;
    }

    public void setSubcategoryStatus(String subcategoryStatus) {
        this.subcategoryStatus = subcategoryStatus;
    }

    @Override
    public String toString() {
        return "Category{" +
                "categoryId=" + categoryId +
                ", categoryName='" + categoryName + '\'' +
                ", categoryPicture='" + categoryPicture + '\'' +
                ", categoryCreatedDate=" + categoryCreatedDate +
                ", categoryUuid='" + categoryUuid + '\'' +
                ", categoryStatus='" + categoryStatus + '\'' +
                ", subcategoryId=" + subcategoryId +
                ", subcategoryName='" + subcategoryName + '\'' +
                ", subcategoryPicture='" + subcategoryPicture + '\'' +
                ", subcategoryCreatedDate=" + subcategoryCreatedDate +
                ", subcategoryUuid='" + subcategoryUuid + '\'' +
                ", subcategoryStatus='" + subcategoryStatus + '\'' +
                '}';
    }
}
