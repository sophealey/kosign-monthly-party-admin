package com.kosign.kmp.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Timestamp;

@Entity
public class Location {

    private String place_name;
    private String address;
    private String websiteUrl;
    private String locationName;
    private String contact;
    private String email;
    private double longitude;
    private double latitude;
    private double rate;
    private String status;
    private Timestamp createdDate;
    private String fileName;
    private String tagNames;
    @Id
    private String uuid;

    public Location() {
    }

    public Location(String place_name, String address, String websiteUrl, String locationName, String contact, String email, double longitude, double latitude, double rate, String status, Timestamp createdDate, String fileName, String tagNames, String uuid) {
        this.place_name = place_name;
        this.address = address;
        this.websiteUrl = websiteUrl;
        this.locationName = locationName;
        this.contact = contact;
        this.email = email;
        this.longitude = longitude;
        this.latitude = latitude;
        this.rate = rate;
        this.status = status;
        this.createdDate = createdDate;
        this.fileName = fileName;
        this.tagNames = tagNames;
        this.uuid = uuid;
    }

    public String getPlace_name() {
        return place_name;
    }

    public void setPlace_name(String place_name) {
        this.place_name = place_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getWebsiteUrl() {
        return websiteUrl;
    }

    public void setWebsiteUrl(String websiteUrl) {
        this.websiteUrl = websiteUrl;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getTagNames() {
        return tagNames;
    }

    public void setTagNames(String tagNames) {
        this.tagNames = tagNames;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
