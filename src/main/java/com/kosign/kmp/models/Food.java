package com.kosign.kmp.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Timestamp;

@Entity
public class Food {
    @Id
    private int         id;
    private String      foodName;
    private double      price;
    private String      availableOn;
    private Timestamp   createdDate;
    private String      uuid;
    private String      status;
    private String      fileName;
    private String      categoryName;
    private String      categoryUuid;
    private String      placeName;
    private String      placeUuid;
    private String      tagNames;


    public Food() {
    }

    public Food(int id, String foodName, double price, String availableOn, Timestamp createdDate, String uuid, String status, String fileName, String categoryName, String categoryUuid, String placeName, String placeUuid, String tagNames) {
        this.id = id;
        this.foodName = foodName;
        this.price = price;
        this.availableOn = availableOn;
        this.createdDate = createdDate;
        this.uuid = uuid;
        this.status = status;
        this.fileName = fileName;
        this.categoryName = categoryName;
        this.categoryUuid = categoryUuid;
        this.placeName = placeName;
        this.placeUuid = placeUuid;
        this.tagNames = tagNames;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getAvailableOn() {
        return availableOn;
    }

    public void setAvailableOn(String availableOn) {
        this.availableOn = availableOn;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryUuid() {
        return categoryUuid;
    }

    public void setCategoryUuid(String categoryUuid) {
        this.categoryUuid = categoryUuid;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public String getPlaceUuid() {
        return placeUuid;
    }

    public void setPlaceUuid(String placeUuid) {
        this.placeUuid = placeUuid;
    }

    public String getTagNames() {
        return tagNames;
    }

    public void setTagNames(String tagNames) {
        this.tagNames = tagNames;
    }

    @Override
    public String toString() {
        return "Food{" +
                "id=" + id +
                ", foodName='" + foodName + '\'' +
                ", price=" + price +
                ", availableOn='" + availableOn + '\'' +
                ", createdDate=" + createdDate +
                ", uuid='" + uuid + '\'' +
                ", status='" + status + '\'' +
                ", fileName='" + fileName + '\'' +
                ", categoryName='" + categoryName + '\'' +
                ", categoryUuid='" + categoryUuid + '\'' +
                ", placeName='" + placeName + '\'' +
                ", placeUuid='" + placeUuid + '\'' +
                ", tagNames='" + tagNames + '\'' +
                '}';
    }
}
