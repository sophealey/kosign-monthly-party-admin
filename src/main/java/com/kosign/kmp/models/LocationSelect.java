package com.kosign.kmp.models;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class LocationSelect {
    @Id
    private String uuid;
    private String placeName;

    public LocationSelect() {
    }

    public LocationSelect(String uuid, String placeName) {
        this.uuid = uuid;
        this.placeName = placeName;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    @Override
    public String toString() {
        return "LocationSelect{" +
                "uuid='" + uuid + '\'' +
                ", placeName='" + placeName + '\'' +
                '}';
    }
}
